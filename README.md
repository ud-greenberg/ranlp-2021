# Code for reproduction of results presented in "Investigating Dominant Word-Order on Universal Dependencies with Graph Rewriting"

This folder contains all the code needed to reproduce the experiments presented in the article "Investigating Dominant Word-Order on Universal Dependencies with Graph Rewriting".

The production of the `tsv` files below need the Grew software to be installed.
We provide them so that it is possible to run the python script without running the Grew part.
The following files are provided because they need the Grew software to be installed

## Main results

### Download UD version 2.7

NB: the Yoruba file contains two typos which should be fixed before the next step. We apply a patch for this.

 * `wget https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3424/ud-treebanks-v2.7.tgz`
 * `tar zxvf ud-treebanks-v2.7.tgz`

### Building corpora with the `isubj` relation
With `grew` version 1.4 installed (see http://grew.fr), run the script:

 * `./src/build_isubj_corpora.sh `

### Compile the corpora
It is required to compile the corpora before counting pattern occurrences in it.

 * `grew compile -i src/isubj_ud-treebanks-v2.7.json`

### Count the pattern occurrences

 * `grew count -patterns "src/isubj/svo.pat src/isubj/sov.pat src/isubj/vso.pat src/isubj/vos.pat src/isubj/osv.pat src/isubj/ovs.pat" -i src/isubj_ud-treebanks-v2.7.json > counts_with_isubj.tsv`

The command above produces a TSV file:

```
Corpus	# sentences	svo	sov	vso	vos	osv	ovs
UD_Afrikaans-AfriBooms	1934	217	1258	24	0	9	0
…
UD_Yoruba-YTB	318	394	9	1	0	19	0
```

### Classify the corpora with more than 1000 sentences

Compute the ratio rank1 / rank2. If it is greater than 2 then rank1 is the dominant order, else there is no dominant order (`ndo` is printed with Rank1 and Rank2 values)

 * `python src/get_dominant_order.py counts_with_isubj.tsv`

### Cosine value

 * `python src/cosine.py counts_with_isubj.tsv` &rarr; print the minimal cosine value for languages with more than one corpora (Figure 4 in article)
 * `python src/cosine.py counts_with_isubj.tsv English` &rarr; print all cosine values for pairs of corpora (values used in heatmaps)

## Other results

### Computing without `isubj`

The same commands and scripts can be used on data without the `isubj` relations:

  * `grew compile -i src/ud-treebanks-v2.7.json`
  * Count (the same patterns can be reused safely as there is no `isubj` relations in the data, only `nsubj` will be tatken into account)
   `grew count -patterns "src/isubj/svo.pat src/isubj/sov.pat src/isubj/vso.pat src/isubj/vos.pat src/isubj/osv.pat src/isubj/ovs.pat" -i src/ud-treebanks-v2.7.json > counts_without_isubj.tsv`
  * Classify: `python src/get_dominant_order.py counts_without_isubj.tsv`
  * Cosine: `python src/cosine.py counts_without_isubj.tsv`

### Analysis about Hindi

 * `grew count -patterns "src/isubj_nominal_for_hindi/svo.pat src/isubj_nominal_for_hindi/sov.pat src/isubj_nominal_for_hindi/vso.pat src/isubj_nominal_for_hindi/vos.pat src/isubj_nominal_for_hindi/osv.pat src/isubj_nominal_for_hindi/ovs.pat" -i src/isubj_ud-Hindi-v2.7.json > counts_with_isubj_nominal_hindi.tsv`
 * ` python src/cosine.py counts_with_isubj_nominal_hindi.tsv` &rarr;  0.993292
 * Ratio of SVO with a verbal object:
   * SVO with verbal object: `grew count -pattern src/svo_with_verbal_object.pat -i src/isubj_ud-Hindi-v2.7.json | grep HDTB | cut -f 3` &rarr; 3302
   * All SVO: `cat counts_with_isubj.tsv | grep HDTB | cut -f 3` &rarr; 4002
   * ratio = 3302 / 4002 = 82.5%

### Analysis about Greek

 * `mkdir UD_Ancient_Greek-Histories`
 * `mkdir UD_Ancient_Greek-Bible`
 * `head -n 7292 isubj_ud-treebanks-v2.7/UD_Ancient_Greek-PROIEL/grc_proiel-ud-dev.conllu | tail -n 7291 > UD_Ancient_Greek-Histories/test.conllu`
 * `head -n 7065 isubj_ud-treebanks-v2.7/UD_Ancient_Greek-PROIEL/grc_proiel-ud-test.conllu | tail -n 7064 >> UD_Ancient_Greek-Histories/test.conllu`
 * `head -n 89910 isubj_ud-treebanks-v2.7/UD_Ancient_Greek-PROIEL/grc_proiel-ud-train.conllu | tail -n 89909 >> UD_Ancient_Greek-Histories/test.conllu`

 * `tail -n 10437 isubj_ud-treebanks-v2.7/UD_Ancient_Greek-PROIEL/grc_proiel-ud-dev.conllu > UD_Ancient_Greek-Bible/test.conllu`
 * `tail -n 10438 isubj_ud-treebanks-v2.7/UD_Ancient_Greek-PROIEL/grc_proiel-ud-test.conllu >> UD_Ancient_Greek-Bible/test.conllu`
 * `tail -n 157180 isubj_ud-treebanks-v2.7/UD_Ancient_Greek-PROIEL/grc_proiel-ud-train.conllu >> UD_Ancient_Greek-Bible/test.conllu`

 * `grew compile -i src/ancient_greek.json`
 * `grew count -patterns "src/isubj/svo.pat src/isubj/sov.pat src/isubj/vso.pat src/isubj/vos.pat src/isubj/osv.pat src/isubj/ovs.pat" -i src/ancient_greek.json > ancient_greek.tsv`
 * `python src/cosine.py ancient_greek.tsv`
