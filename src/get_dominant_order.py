#!/usr/bin/env python3

import sys
import os
import csv

def update_corpus(corpus):
    # total = int(corpus["svo"]) + int(corpus["sov"])  + int(corpus["vso"])  + int(corpus["vos"])  + int(corpus["osv"])  + int(corpus["ovs"])
    # corpus["total"] = total
    # corpus["svo_ratio"] = int(corpus["svo"])/total
    # corpus["sov_ratio"] = int(corpus["sov"])/total
    # corpus["vso_ratio"] = int(corpus["vso"])/total
    # corpus["vos_ratio"] = int(corpus["vos"])/total
    # corpus["osv_ratio"] = int(corpus["osv"])/total
    # corpus["ovs_ratio"] = int(corpus["ovs"])/total

    orders = [
        ("svo", int(corpus["svo"])),
        ("sov", int(corpus["sov"])),
        ("vso", int(corpus["vso"])),
        ("vos", int(corpus["vos"])),
        ("osv", int(corpus["osv"])),
        ("ovs", int(corpus["ovs"])),
     ]
    sorted_orders = sorted(orders, key = lambda x: x[1], reverse=True)
    if sorted_orders[1][1] == sorted_orders[0][1]:
        raise Exception('equal ranks 1 and 2')
    inv_ratio = sorted_orders[1][1] / sorted_orders[0][1]

    if sorted_orders[1][1] != 0:
        ratio = sorted_orders[0][1] / sorted_orders[1][1]
    else:
        ratio = 0
    corpus["inv_ratio"] = inv_ratio
    if inv_ratio < 0.5:
        #corpus["class"] = "%s, ratio = %s" %(sorted_orders[0][0], ratio)
        corpus["class"] = "%s, %s" %(sorted_orders[0][0], ratio)
    else:
        if sorted_orders[2][1] == sorted_orders[1][1]:
            raise Exception('equal ranks 2 and 3')
        #corpus["class"] = "ndo (%s/%s), ratio = %s" % (sorted_orders[0][0], sorted_orders[1][0], ratio)
        corpus["class"] = "ndo (%s/%s), %s" % (sorted_orders[0][0], sorted_orders[1][0], ratio)
    return (corpus)

def get_dominant_order(corpus_csv):
    with open(corpus_csv, newline='') as csvfile:
        reader = csv.DictReader(csvfile, dialect='excel-tab')
        # all lines
        corpus_list = list (reader)
        # filter above the 1K sentence threshold
        corpus_1K = [c for c in corpus_list if int (c["# sentences"]) >= 1000]
        #
        ucorp = [update_corpus(c) for c in corpus_1K]
        for c in ucorp:
            #print ("%s --> %s" % (c["Corpus"], c["class"]))
            print ("%s, %s" % (c["Corpus"], c["class"]))

if len (sys.argv) != 2:
    print ("Error: one argument expected")
else:
    get_dominant_order (sys.argv[1])
