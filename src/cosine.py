#!/usr/bin/env python3

import sys
import os
import csv
import math

def get_language (corpus):
    return (corpus.split("-")[0][3:])

def norm (c):
    return math.sqrt(int (c["svo"])**2+int (c["sov"])**2+int (c["vso"])**2+int (c["vos"])**2+int (c["osv"])**2+int (c["ovs"])**2)

def cosine (c1, c2):
    return (
        (int (c1["svo"]) * int (c2["svo"])+
        int (c1["sov"]) * int (c2["sov"])+
        int (c1["vso"]) * int (c2["vso"])+
        int (c1["vos"]) * int (c2["vos"])+
        int (c1["osv"]) * int (c2["osv"])+
        int (c1["ovs"]) * int (c2["ovs"]))/
        (norm(c1) * norm(c2))
    )

def load (corpus_csv):
    with open(corpus_csv, newline='') as csvfile:
        reader = csv.DictReader(csvfile, dialect='excel-tab')
        # all lines
        corpus_list = list (reader)
        # filter above the 1K sentence threshold
        corpus_1K = [c for c in corpus_list if int (c["# sentences"]) >= 1000]
    return corpus_1K

def language_cosine(language_corpora):
    smallest = 1
    for c1 in language_corpora:
        for c2 in language_corpora:
            cos = cosine(c1,c2)
            # print ("(%s/%s) --> %f" % (c1["Corpus"], c2["Corpus"], cos))
            if cos < smallest:
                smallest = cos
    return smallest


if len (sys.argv) == 2:
    data = load (sys.argv[1])
    all_languages = list(dict.fromkeys([get_language (c["Corpus"]) for c in data]))
    output = []
    for language in all_languages:
        language_corpora = [c for c in data if get_language (c["Corpus"]) == language]
        if len(language_corpora) > 1:
            output += [(language, len(language_corpora), language_cosine(language_corpora))]
    sorted_output = sorted(output, key = lambda x: x[2])
    for item in sorted_output:
        print ("%s --> %d corpora, min cosine is %f" % (item[0], item[1], item[2]))

elif len (sys.argv) == 3:
    data = load (sys.argv[1])
    language_corpora = [c for c in data if get_language (c["Corpus"]) == sys.argv[2]]
    for c1 in language_corpora:
        for c2 in language_corpora:
            cos = cosine(c1,c2)
            print ("(%s/%s) --> %f" % (c1["Corpus"], c2["Corpus"], cos))

else:
    print ("Error: one argument expected")
