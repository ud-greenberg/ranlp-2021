pattern {
  V [upos=VERB];
  V -[1=nsubj|isubj, !enhanced]-> S; S [upos=NOUN|PROPN|PRON];
  V -[1=obj, !enhanced]-> O; O [upos=NOUN|PROPN|PRON];
  S << V; V << O;
}
