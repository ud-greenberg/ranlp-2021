pattern {
  V [upos=VERB];
  V -[1=nsubj|isubj, !enhanced]-> S;
  V -[1=obj, !enhanced]-> O;
  O << V; V << S;
}
