
OUT="isubj_ud-treebanks-v2.7"

mkdir -p $OUT

for dir in ud-treebanks-v2.7/*
do
  corpus=`basename ${dir}`
  echo "building $corpus"
  mkdir -p $OUT/$corpus
    for full_file in ${dir}/*.conllu
    do
       file=`basename ${full_file}`
       out_file="$OUT/${corpus}/${file}"
       echo "--->$full_file-->$out_file"
       grew transform -grs src/isubj.grs -i $full_file -o $out_file
    done
done
